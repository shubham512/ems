package ems.giranarsoft.emsproject.database;

public class Constant {
	public static interface Queries {
		public static final String INSERT_EMPLOYEE = "INSERT INTO `ems`.`employee` ("
				+"  `first_name`,"
				+"  `last_name`,"
				+"  `des_id`,"
				+"  `pin`,"
				+"  `created_at`,"
				+"  `updated_at`"
				+") "
				+"VALUES"
				+"  ("
				+"    ?,"
				+"    ?,"
				+"    ?,"
				+"    ?,"
				+"   NOW(),"
				+"    NOW()"
				+"  );";
		public static final String INSERT_DESIGNATION = "INSERT INTO `ems`.`designation` ("
				+"  `des_name`,"
				+"  `created_at`,"
				+"  `updated_at`"
				+") "
				+"VALUES"
				+"  ("
				+"    ?,"
				+"   NOW(),"
				+"    NOW()"
				+"  );";
	}
}
