package ems.girnarsoft.emsproject.employee_service;

import com.mysql.jdbc.Connection;

import ems.giranarsoft.emsproject.database.EmployeeDatabase;

public interface EmployeeService {
	public void promoteEmployee(Employee employee, Connection connection);
	public void displayMyMentor(Employee employee, Connection connection);
	public void displayEmployeesUnderMe(Employee employee, Connection connection);
	public void addMyMentor(Employee employee, Connection connection);
	public void addEmployeeUnderMe(Employee employee, Connection connection);
}
 