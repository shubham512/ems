/**
* This Class is Actually a Child class of Employee Class.
*/
package ems.girnarsoft.emsproject.employee_service;

import ems.giranarsoft.emsproject.database.EmployeeDatabase;
import ems.girnarsoft.emsproject.service.ServiceClass;
import java.io.IOException;
import java.util.LinkedList;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.mysql.jdbc.Connection;

public class CeoService implements EmployeeService {
    
    /*public Ceo(String firstName, String lastName, String designationOfEmployee, int pin, int employeeId) {
        super(firstName, lastName, designationOfEmployee, pin, employeeId);
    }
    
    public Ceo(int employeeId, String firstName, String lastName, String designationOfEmployee, int pin) {
        super(employeeId, firstName, lastName, designationOfEmployee, pin);
    }*/
    
    public void promoteEmployee(Employee employee, Connection connection) {
        
        ServiceClass serviceObject = new ServiceClass();
        try {
            serviceObject.promoteEmployee(employee, connection);
        } catch (IOException ex) {
            Logger.getLogger(CeoService.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    public void displayMyMentor(Employee employee, Connection connection) {
        
        ServiceClass serviceObject = new ServiceClass();
        try {
            serviceObject.displayMyMentor(employee, connection);
        } catch (IOException ex) {
            Logger.getLogger(CeoService.class.getName()).log(Level.SEVERE, null, ex);
        }
      
    }
    
    public void displayEmployeesUnderMe(Employee employee, Connection connection) {
        
        ServiceClass serviceObject = new ServiceClass();
        try {
            serviceObject.displayEmployeesUnderMe(employee, connection);
        } catch (IOException ex) {
            Logger.getLogger(CeoService.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    public void addMyMentor(Employee employee, Connection connection) {
        
        ServiceClass serviceObject = new ServiceClass();
        try {
            serviceObject.addMyMentor(employee, connection);
        } catch (IOException ex) {
            Logger.getLogger(CeoService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void addEmployeeUnderMe(Employee employee, Connection connection) {
        
        ServiceClass serviceObject = new ServiceClass();
        try {
            serviceObject.addEmployeeUnderMe(employee, connection);
        } catch (IOException ex) {
            Logger.getLogger(CeoService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
