/**
* This Class is meant for the purpose of serving all the feature of EMS to all the Employee.
* This class consist services like
* Promotion of an Employee.
* Display all the Mentor of a particular Employee.
* Display all the Employee who are working under a particular Employee.
* Add Mentor for an Employee.
* Add all the Employee who are working under a particular Employee.
*/
package ems.girnarsoft.emsproject.service;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

import com.mysql.jdbc.Connection;

import ems.girnarsoft.emsproject.employee_service.Employee;

public class ServiceClass {
    
    /**
    * This Method will promote an Employee just by changing their Designation in all the files.
    * 
    */
    public void promoteEmployee(Employee employee, Connection connection) throws IOException {
    	PreparedStatement pstmt = null ;
        ResultSet rs = null ;
        Scanner scanner = new Scanner(System.in);
        if(employee.getDesignation().equals("New Joiner"))
        {
        	System.out.println("You are a newbie, you can not promote any Employee.");
        }
        
        else if(employee.getDesignation().equals("Manager")) 
        {
            boolean flag = true;
            while(flag) {
                System.out.println("Enter The Employee Id Whom you want to promote");
                int promotingEmployeeId = validateInteger();
       
                if(!isEmployeeExist(promotingEmployeeId, connection)) 
                {
                    System.out.println("No Employee Exists with given Employee ID.");
                    System.out.println("Please Enter the Existing Employee ID");
                }
                else
                {
                    flag = false;
                    if(getEmployeeDesignation(promotingEmployeeId,connection).equals("New Joiner")) 
                    {
                        promoteNewJoiner(promotingEmployeeId,connection);
                    }
                    else
                    {
                        System.out.println("You are not Authorized to promote The " + getEmployeeDesignation(promotingEmployeeId, connection));
                    }
                }
            }
            
        }
        else if(employee.getDesignation().equals("Director") || employee.getDesignation().equals("HR"))
        {
            boolean flag = true;
            while(flag) {
                System.out.println("Enter The Employee Id Whom you want to promote");
                int promotingEmployeeId = validateInteger();

                if(!isEmployeeExist(promotingEmployeeId, connection)) 
                {
                    System.out.println("No Employee Exists with given Employee ID.");
                    System.out.println("Please Enter the Existing Employee ID");
                }
                else
                {
                    flag = false;
                    if(getEmployeeDesignation(promotingEmployeeId,connection).equals("New Joiner")) 
                    {
                    	promoteNewJoiner(promotingEmployeeId,connection);
                    }
                    else if(getEmployeeDesignation(promotingEmployeeId,connection).equals("Manager")) 
                    {
                        promoteManager(promotingEmployeeId,connection);
                    }
                    else
                    {
                        System.out.println("You are not Authorized to promote The " + getEmployeeDesignation(promotingEmployeeId, connection));
                    }
                }
            }
        }
        else if(employee.getDesignation().equals("CEO"))
        {
            boolean flag = true;
            while(flag) {
                System.out.println("Enter The Employee Id Whom you want to promote");
                int promotingEmployeeId = validateInteger();

                if(!isEmployeeExist(promotingEmployeeId, connection)) 
                {
                    System.out.println("No Employee Exists with given Employee ID.");
                    System.out.println("Please Enter the Existing Employee ID");
                }
                else
                {
                    flag = false;
                    if(getEmployeeDesignation(promotingEmployeeId,connection).equals("New Joiner")) 
                    {
                    	promoteNewJoiner(promotingEmployeeId,connection);
                    }
                    else if(getEmployeeDesignation(promotingEmployeeId,connection).equals("Manager")) 
                    {
                        promoteManager(promotingEmployeeId,connection);
                    }
                    else if(getEmployeeDesignation(promotingEmployeeId,connection).equals("HR"))
                    {
                        System.out.println(getEmployeeName(promotingEmployeeId, connection) + " is HR of The Company. And HR can Not be promoted.");
                    }
                    else if(getEmployeeDesignation(promotingEmployeeId,connection).equals("Director"))
                    {
                        System.out.println(getEmployeeName(promotingEmployeeId, connection) + " is Director of The Company. And Director can Not be promoted.");
                    }
                    else
                    {
                        System.out.println(getEmployeeName(promotingEmployeeId, connection) + " is the CEO of the Company. And CEO can not be promoted.");
                    }
                }
                
            }
        }
    }
    
    /**
    * This Method will display all the mentors of an EMployee.
    */
    public void displayMyMentor(Employee employee, Connection connection) throws IOException {
        
    	int employeeId = employee.getEmployeeId();
    	PreparedStatement pstmt = null ;
        ResultSet rs = null ;
    	try {
			pstmt = connection.prepareStatement("select count(*), emp_id, first_name, last_name from employee where emp_id in (select mentor_id from employee where emp_id = " +  Integer.toString(employeeId) + ") ;" );
			rs = pstmt.executeQuery();
			rs.next();
			if(rs.getInt(1) > 0)
			{
				System.out.println("Mentor Id | First name | LastName");
//				System.out.println(rs.getInt(2) + "|" + rs.getString(3) + "|" + rs.getString(4));
				System.out.format("%5d%15s%15s", rs.getInt(2), rs.getString(3), rs.getString(4));
			}
			else
	        {
	            System.out.println("You don't have any Mentor.");
	        }
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
        
        
    }
    
    /**
    * This Method will display all the EMployee who are working under an EMployee.
    */
    public void displayEmployeesUnderMe(Employee employee, Connection connection) throws IOException {
        
    	int employeeId = employee.getEmployeeId();
    	PreparedStatement pstmt = null ;
        ResultSet rs = null ;
    	try {
			pstmt = connection.prepareStatement("select emp_id, first_name, last_name from employee where mentor_id = " +  Integer.toString(employeeId) + ";" );
			rs = pstmt.executeQuery();
			
			if(rs.next())
			{
				System.out.format("%5s%15s%15s\n", "EmployeeId", "FirstName", "LastName");
				System.out.format("%5d%15s%15s\n", rs.getInt(1), rs.getString(2), rs.getString(3));
				while(rs.next()) {
					
//					System.out.println(rs.getInt(1) + "|" + rs.getString(2) + "|" + rs.getString(3));
					System.out.format("%5d%15s%15s\n", rs.getInt(1), rs.getString(2), rs.getString(3));
				}
			}
			else
	        {
				System.out.println("There is no Employee who works under you.");
	        }
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        
    }
    
    /**
    * This Method will add the mentor for an Employee.
    */
    public void addMyMentor(Employee employee, Connection connection) throws IOException {
        Scanner scanner = new Scanner(System.in);
    	int employeeId = employee.getEmployeeId();
    	String employeeDesignation = getEmployeeDesignation(employeeId, connection);
    	PreparedStatement pstmt = null ;
        ResultSet rs = null ;
    	try {
			pstmt = connection.prepareStatement("select mentor_id from employee where emp_id = " +  Integer.toString(employeeId) + ";" );
			rs = pstmt.executeQuery();
			rs.next();
			if(rs.getInt(1) != 0)
			{
				System.out.println("Employee Id " + rs.getInt(1) + " is Already your Mentor.");
				System.out.println("Do you want to update your mentor ?");
				boolean flag = true;
				while(flag) {
					System.out.println("If yes press - 1, Otherwise press- 0");
					int choice = validateInteger();
					if(choice == 1)
					{
						flag= false;
						addNewMentor(connection,employeeId,employeeDesignation);
						
					}
					else if(choice == 0)
					{
						flag = false;
						
					}
					else
					{
						flag = true;
					}
						
				}
			}
			else
	        {
				addNewMentor(connection, employeeId,employeeDesignation);
	        }
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
           
        
    }
    
    /**
    * This Method will add employee who are working under an employee.
    */
    public void addEmployeeUnderMe(Employee employee, Connection connection) throws IOException {
        
    	Scanner scanner = new Scanner(System.in);
    	int employeeId = employee.getEmployeeId();
    	String employeeDesignation = getEmployeeDesignation(employeeId, connection);
    	PreparedStatement pstmt = null ;
        ResultSet rs = null ;
    	try {
    		boolean flag = true;
            while(flag) {
                System.out.println("Enter your Employee Id whom you want to add under you.");
                int underEmployeeID = validateInteger();
                pstmt = connection.prepareStatement("select first_name from employee where emp_id = " +  Integer.toString(underEmployeeID) + ";" );
    			rs = pstmt.executeQuery();
                if (rs.next()) 
                {
                    flag = false;
                    String underEmployeeDesignation = getEmployeeDesignation(underEmployeeID, connection);
                    if ((employeeDesignation.equals("CEO") && !underEmployeeDesignation.equals("CEO")) || (employeeDesignation.equals("HR") && !underEmployeeDesignation.equals("CEO") && !underEmployeeDesignation.equals("Director") && !underEmployeeDesignation.equals("HR")) || (employeeDesignation.equals("Director") && !underEmployeeDesignation.equals("CEO") && !underEmployeeDesignation.equals("HR") && !underEmployeeDesignation.equals("Director")) || (employeeDesignation.equals("Manager") && !underEmployeeDesignation.equals("CEO") && !underEmployeeDesignation.equals("HR") && !underEmployeeDesignation.equals("Director") && !underEmployeeDesignation.equals("Manager")) || (employeeDesignation.equals("New Joiner") && !underEmployeeDesignation.equals("CEO") && !underEmployeeDesignation.equals("Director") && !underEmployeeDesignation.equals("HR") && !underEmployeeDesignation.equals("Manager") && !underEmployeeDesignation.equals("New Joiner")))
                    {
                        addNewEmployeeUnderMe(connection, employeeId, underEmployeeID);
                    }
                    else
                    {
                        System.out.println("You can not add your colleague, Senior or yourself as Employee who works under you");
                        System.out.println("Employee Id " + underEmployeeID + ", " + getEmployeeName(underEmployeeID, connection) + " is The " + underEmployeeDesignation + " of the compnay.");
                        System.out.println("Thank You !!");
                    }
                }
                else
                {
                	System.out.println("No Employee Exists with given Employee ID.");
                    System.out.println("Please Enter the Existing Employee ID");
                }
            }    
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    }
    
    /**
    * This Method is used for the performance of the code, so that the same code should not repeate.
    */
    private void addNewEmployeeUnderMe(Connection connection, int employeeId, int underEmployeeID) throws IOException {
        
    	PreparedStatement pstmt = null ;
        ResultSet rs = null ;
        try {
			pstmt = connection.prepareStatement("update employee set mentor_id = " + Integer.toString(employeeId) + " where emp_id = " +  Integer.toString(underEmployeeID) + ";" );
			pstmt.executeUpdate();
			System.out.println("You have Successfully added " + getEmployeeName(underEmployeeID, connection) + ".");
	   
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
    }
    
    /**
    * This Method is used for the performance of the code, so that the same code should not repeate.
    */
    private void addNewMentor(Connection connection, int employeeId, String employeeDesignation) throws IOException {
         
    	PreparedStatement pstmt = null ;
        ResultSet rs = null ;
    	boolean flag2 = true;
		while(flag2) {
			System.out.println("Enter your mentor Id");
            int mentorID = validateInteger();
            try {
				pstmt = connection.prepareStatement("select first_name from employee where emp_id = " +  Integer.toString(mentorID) + ";" );
				rs = pstmt.executeQuery();
				if(rs.next())
				{
					flag2 = false;
					String mentorDesignation = getEmployeeDesignation(mentorID, connection);
					if ((employeeDesignation.equals("New Joiner") && !mentorDesignation.equals("New Joiner")) || (employeeDesignation.equals("Manager") && !mentorDesignation.equals("Manager") && !mentorDesignation.equals("New Joiner")) || (employeeDesignation.equals("Director") && !mentorDesignation.equals("Director") && !mentorDesignation.equals("Manager") && !mentorDesignation.equals("New Joiner")) || (employeeDesignation.equals("HR") && !mentorDesignation.equals("HR") && !mentorDesignation.equals("Manager") && !mentorDesignation.equals("New Joiner")) || (employeeDesignation.equals("CEO") && !mentorDesignation.equals("CEO") && !mentorDesignation.equals("Director") && !mentorDesignation.equals("HR") && !mentorDesignation.equals("Manager") && !mentorDesignation.equals("New Joiner")))
	                {
						pstmt = connection.prepareStatement("update employee set mentor_id = " + Integer.toString(mentorID) + " where emp_id = " +  Integer.toString(employeeId) + ";" );
						pstmt.executeUpdate();
						System.out.println("You have successfully Updated your Mentor !!");
	                }
	                else
	                {
	                    System.out.println("You can not add your colleague, Junior or Yourself as your mentor");
	                    System.out.println("Employee Id " + mentorID + ", " + getEmployeeName(mentorID, connection) + " is The " + mentorDesignation + " of the compnay.");
	                    System.out.println("Thank You !!");
	                }
					
				}
				else
				{
					System.out.println("No Employee Exists with given Employee ID.");
	                System.out.println("Please Enter the Existing Employee ID");
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
    }
    
    /**
    * This Method is used for the performance of the code, so that the same code should not repeate.
    */
    private void promoteNewJoiner(int promotingEmployeeId, Connection connection) throws IOException {
        
        System.out.println("You can promote Mr. " + getEmployeeName(promotingEmployeeId, connection) + " as a Manager");
        System.out.println("If you want to promote him as a manager press 1 other wise 0");
        int directorChoice = validateInteger();
        if(directorChoice == 1) 
        {
        	PreparedStatement pstmt = null ;
            ResultSet rs = null ;
        	try {
    			pstmt = connection.prepareStatement("update employee set des_id = (select des_id from designation where des_name = 'Manager') where emp_id = " +  Integer.toString(promotingEmployeeId) + ";" );
    			pstmt.executeUpdate();
    			System.out.println("You Have Successfully Promoted " + getEmployeeName(promotingEmployeeId, connection) + " as a Manager");
    		} catch (SQLException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		}
            
        }
    }
    
    /**
    * This Method is used for the performance of the code, so that the same code should not repeate.
    */
    private void promoteManager(int promotingEmployeeId, Connection connection) throws IOException {
        
        System.out.println("You can promote Mr. " + getEmployeeName(promotingEmployeeId, connection) + " as a Director");
        System.out.println("If you want to promote him as a Director press 1 other wise 0");
        int directorChoice = validateInteger();
        if(directorChoice == 1) {
        	PreparedStatement pstmt = null ;
            ResultSet rs = null ;
        	try {
    			pstmt = connection.prepareStatement("update employee set des_id = (select des_id from designation where des_name = 'Director') where emp_id = " +  Integer.toString(promotingEmployeeId) + ";" );
    			pstmt.executeUpdate();
    			System.out.println("You Have Successfully Promoted " + getEmployeeName(promotingEmployeeId, connection) + " as a Directors");
    		} catch (SQLException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		}
            
        }
    }
    
    private boolean isEmployeeExist(int employeeId, Connection connection) {
    	
    	PreparedStatement pstmt = null ;
        ResultSet rs = null ;
    	try {
			pstmt = connection.prepareStatement("select count(*) from employee where emp_id = " +  Integer.toString(employeeId) + ";" );
			rs = pstmt.executeQuery();
			rs.next();
			if(rs.getInt(1) == 0)
			{
				return false;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	return true;
		
    }
    
    private String getEmployeeDesignation(int employeeId, Connection connection) {
    	PreparedStatement pstmt = null ;
        ResultSet rs = null ;
        String designationOfEmployee = "";
    	try {
			pstmt = connection.prepareStatement("select des.des_name from designation des join employee emp on emp.des_id = des.des_id where emp.emp_id =  " +  Integer.toString(employeeId) + ";" );
			rs = pstmt.executeQuery();
			rs.next();
			designationOfEmployee = rs.getString(1);
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	return designationOfEmployee;
    }
    
    private String getEmployeeName(int employeeId, Connection connection) {
    	PreparedStatement pstmt = null ;
        ResultSet rs = null ;
        String name = "";
    	try {
			pstmt = connection.prepareStatement("select first_name,last_name from employee where emp_id = " +  Integer.toString(employeeId) + ";" );
			rs = pstmt.executeQuery();
			rs.next();
			name += rs.getString(1) + " " + rs.getString(2);
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	return name;
    	
    }
    
    private int validateInt() {
        Scanner scanner = new Scanner(System.in);
        while (!scanner.hasNextInt()) {
            System.out.println("That's not a  Valid number!");
            System.out.println("Please Enter Only Valid Integer");
            System.out.println("");
            scanner.next(); // this is important!
        }
        return scanner.nextInt();
    }
    
    private static int validateInteger() {
        Scanner scanner = new Scanner(System.in);
        String input = "";
        boolean flag = true;
        while(flag)
        {
            input = scanner.nextLine();
            char[] ch = input.toCharArray();
            for(int index = 0; index < input.length(); index++) 
            {
                if(ch[index] == '0' || (ch[index] >= '1' && ch[index] <= '9'))
                {
                    flag = false;
                    continue;
                }
                else
                {
                    System.out.println("That's not a  Valid number!");
                    System.out.println("Please Enter Only Valid Integer");
                    System.out.println("");
                    flag = true;
                    break;
                }
            }
        }
        return Integer.parseInt(input);
        
    }
}
