/**
* This class is meant for the starting the Application.
* It Contains Main Method, and some basic features Like,
* SignUp, LogIn, and choosing Activity
*/
package ems.girnarsoft.emsproject;
import ems.giranarsoft.emsproject.database.ConnectToDatabase;
import ems.giranarsoft.emsproject.database.Constant;
import ems.giranarsoft.emsproject.database.EmployeeDatabase;
import ems.girnarsoft.emsproject.employee_service.*;

import java.io.IOException;
import java.util.Map;
import java.util.Scanner;

import com.mysql.jdbc.Connection;

import java.sql.*;

public class ExecutionClass {
    
    public static void main (String[] args) throws IOException {
       Scanner scanner = new Scanner(System.in);
       System.out.println("*************************************");
       System.out.println("Welcome to Employee Management System");
     
       
       Connection connection = (Connection) ConnectToDatabase.getConnection();
       PreparedStatement pstmt = null ;
       ResultSet rs = null ;
       
       boolean isTrue = true;
       while(isTrue)
       {
            
                System.out.println("Press 1 - SignUp");
                System.out.println("Press 2 - Login");
                System.out.println("Press 3 - Exit");
                int userChoice;
                userChoice = validateInteger();
                switch (userChoice) {
                case 1:
                    signUp(connection); 
                    break;

                case 2:
                    signIn(connection);    
                    break;

                case 3:
                    isTrue = false;
                    /**
                    * Write into file after terminating the program
                    */
                    try {
						connection.close();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}  
                    break;
                }
                
       }
       
       
       
   }
    /**
    * SignUp Method is meant for the Adding a new Employee into the database(files).
    */
    public static void signUp(Connection connection) throws IOException {
        
    	PreparedStatement pstmt = null ;
        ResultSet rs = null ;
        Scanner scanner = new Scanner(System.in);
        String firstName,lastName,designationOfEmpployee;
        int mPIN;
        boolean flag = true;
        System.out.println("Please Enter your First Name");
        firstName = validateString();
        System.out.println("Please Enter your Last Name");
        lastName = validateString();
        System.out.println("Please choose your mPIN, Only Integer value is allowed");
        mPIN = validateInteger();
        
        while(flag) {
            
            System.out.println("Please choose your designation");
            System.out.println("1- CEO");
            System.out.println("2- HR");
            System.out.println("3- Director");
            System.out.println("4- Manager");
            System.out.println("5- New Joiner");
            System.out.println("6- Exit");
            int designationChoice = validateInteger();
            Employee employee;
            /*
            * These 5 cases are meant to different type of employee.
            * case 1 - For Creating employee of type CEO.
            * case 2 - FOr Creating employee of type Director.
            * case 3 - FOr Creating employee of type HR.
            * case 4 - FOr Creating employee of type Manager
            * case 5- FOr Creating employee of type New Joiner.
            */
            
            switch(designationChoice) {
                case 1:
                    if(!isCeoExist(connection))
                    {
                        /*employee = new Employee(firstName, lastName, "CEO", mPIN, database.getEmployeeDetailsMap().size());
                        database.getEmployeeDetailsMap().put(employee.getEmployeeId(), employee);
                        database.getEmployeeLoginDetailsMap().put(employee.getEmployeeId(), employee.getPin());
                        database.getEmployeeDesignationDetailsMap().put(employee.getEmployeeId(), "CEO");*/
                    	insertEmployee(firstName, lastName, designationChoice, mPIN, connection);
                        flag = false;
                    }
                    else
                    {
                        System.out.println("CEO Already Exist in the Company Database. You can not sign up as a CEO");
                    }
                    break;

                case 2:
                	insertEmployee(firstName, lastName, designationChoice, mPIN, connection);
                    flag = false;
                    break;

                case 3:
                	insertEmployee(firstName, lastName, designationChoice, mPIN, connection);
                    flag = false;
                    break;

                case 4:
                	insertEmployee(firstName, lastName, designationChoice, mPIN, connection);
                    flag = false;
                    break;

                case 5:
                	insertEmployee(firstName, lastName, designationChoice, mPIN, connection);
                    flag = false;
                    break;
                case 6:
                    flag = false;
                    break;
                default:
                    System.out.println("Please Choose Only given option");
                    break;

            }
        }
        
    }
    /**
    * LogIn Method is meant for the validating employees credencial from the database.
    * Giving Access of EMS System by validating employee.
    */
    private static void signIn(Connection connection) throws IOException {
        
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please Enter your Employee ID");
        int employeeID = validateInteger();
        
        PreparedStatement pstmt = null ;
        ResultSet rs = null ;
        try {
			pstmt = connection.prepareStatement("select first_name from employee where emp_id = " + Integer.toString(employeeID) + ";" );
			rs = pstmt.executeQuery();
			if(rs.next()) {
	            System.out.println("Please Enter your mPIN");
	            int mpin = validateInteger();
	            //System.out.println(database.getEmployeeLoginDetailsMap().get(employeeID));
	            
	            pstmt = connection.prepareStatement("select pin from employee where emp_id = " +  Integer.toString(employeeID) + ";" );
				rs = pstmt.executeQuery();
				rs.next();
				
//	            if(database.getEmployeeLoginDetailsMap().get(employeeID) == mpin) {
				if(rs.getInt(1) == mpin) {
//	                Employee employee = database.getEmployeeDetailsMap().get(employeeID);
	                Employee employee = getEmployeeObject(employeeID, mpin, connection);
	                
					
	                System.out.println("Welcome to EMS System "+employee.getFirstName() + " " + employee.getLastName() + " - " + employee.getDesignation());
	                activity(employee,connection);
	                
	            } else {
	                System.out.println("You Entered wrong mPIN");
	            }
	            
	        } else {
	            System.out.println("You entered wrong Employee ID");
	        }
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
        
        
    }
    /**
    * An Authorized Employee will Able to perform Activity.
    * Activity Like, Promote a Employee.
    * Add your mentor
    * Add Employee Under whom you work.
    * And display all the feasible Information to the user.
    */
    public static void activity(Employee employee, Connection connection) {
        
        boolean isTrue = true;
        EmployeeService service = null;
        if(employee.getDesignation().equals("CEO")) {
        	service = new CeoService();
        }
        if(employee.getDesignation().equals("HR")) {
        	service = new HrService();
        }
        if(employee.getDesignation().equals("Director")) {
        	service = new DirectorService();
        }
        if(employee.getDesignation().equals("Manager")) {
        	System.out.println("here");
        	service = new ManagerService();
        }
        if(employee.getDesignation().equals("New Joiner")) {
        	System.out.println("here");
        	service = new NewJoinerService();
        }
        while(isTrue) {
            
        Scanner scanner = new Scanner(System.in);
        
        System.out.println("");
        System.out.println("");
        System.out.println("Choose a option that you want to do");
        System.out.println("1- Promote a Employee");
        System.out.println("2- Display Employees Who are working under you");
        System.out.println("3- Display Your Mentor");
        System.out.println("4- Add Employee who is working under you");
        System.out.println("5- Add your Mentor");
        System.out.println("6- Exit");
        System.out.println("");
        int activityChoice = validateInteger();
        switch(activityChoice) {
            case 1:
            	service.promoteEmployee(employee,connection);
//                employee.promoteEmployee(database);
                break;
                
            case 2:
            	service.displayEmployeesUnderMe(employee,connection);
//                employee.displayEmployeesUnderMe(database);
                break;
                
            case 3:
            	service.displayMyMentor(employee,connection);
//                employee.displayMyMentor(database);
                break;
                
            case 4:
            	service.addEmployeeUnderMe(employee,connection);
//                employee.addEmployeeUnderMe(database);
                break;
                
            case 5:
            	service.addMyMentor(employee,connection);
//                employee.addMyMentor(database);
                break;
            case 6:
                isTrue = false;
                break;
            default:
                System.out.println("Please choose only given option");
                       
        }
        
        }
           
    }
    
    private static Employee getEmployeeObject(int employeeID, int mpin, Connection connection) {
    
    	PreparedStatement pstmt = null ;
        ResultSet rs = null ;
        Employee employee = null;
    	String first_name, last_name, designationOfEmployee;
        try {
			pstmt = connection.prepareStatement("select des.des_name from designation des join employee emp on emp.des_id = des.des_id where emp_id = " + Integer.toString(employeeID) + ";" );
			rs = pstmt.executeQuery();
			rs.next();
			designationOfEmployee = rs.getString(1);
			pstmt = connection.prepareStatement("select first_name, last_name from employee where emp_id = " + Integer.toString(employeeID) + ";" );
			rs = pstmt.executeQuery();
			rs.next();
			first_name = rs.getString(1);
			last_name = rs.getString(2);
			employee = new Employee(employeeID, first_name, last_name, designationOfEmployee, mpin);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return employee;
    }
    
    private static void insertEmployee(String firstName, String lastName, int designationChoice, int mPIN, Connection connection) {
    	
    	PreparedStatement pstmt = null ;
        ResultSet rs = null ;
    	try {
			pstmt = connection.prepareStatement(Constant.Queries.INSERT_EMPLOYEE, Statement.RETURN_GENERATED_KEYS);
			pstmt.setString(1, firstName);
			pstmt.setString(2, lastName);
			pstmt.setInt(3, designationChoice );
			pstmt.setInt(4, mPIN);
			pstmt.executeUpdate();
			rs = pstmt.getGeneratedKeys();
			int insertedEmployeeId = 0;
			if(rs.next()){
				insertedEmployeeId =rs.getInt(1);
			}
			System.out.println("You have Successfully Signed Up !!");
            System.out.println("Your auto generated Employee Id is : " + insertedEmployeeId);
            System.out.println("Please Remember your Employee Id and Password");
            System.out.println("");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    public static boolean isCeoExist(Connection connection) {
        
        /*for(Map.Entry<Integer,String> myMap : database.getEmployeeDesignationDetailsMap().entrySet())
        {
            if(myMap.getValue().equals("CEO"))
            {
                return true;
            }
        }*/
    	PreparedStatement pstmt = null ;
        ResultSet rs = null ;
        try {
        	pstmt = connection.prepareStatement("select count(*) from employee where des_id = 1 ;" );
			rs = pstmt.executeQuery();
			rs.next();
			if(rs.getInt(1)>0)
			{
				return true;
			}
        } catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        return false;
    }
    
    public static int validateInt() {
        Scanner scanner = new Scanner(System.in);
        while (!scanner.hasNextInt()) {
            System.out.println("That's not a  Valid number!");
            System.out.println("Please Enter Only Valid Integer");
            System.out.println("");
            scanner.next(); // this is important!
        }
        return scanner.nextInt();
    }
    
    public static int validateInteger() {
        Scanner scanner = new Scanner(System.in);
        String input = "";
        boolean flag = true;
        while(flag)
        {
            input = scanner.nextLine();
            char[] ch = input.toCharArray();
            for(int index = 0; index < input.length(); index++) 
            {
                if(ch[index] == '0' || (ch[index] >= '1' && ch[index] <= '9'))
                {
                    flag = false;
                    continue;
                }
                else
                {
                    System.out.println("That's not a  Valid number!");
                    System.out.println("Please Enter Only Valid Integer");
                    System.out.println("");
                    flag = true;
                    break;
                }
            }
        }
        return Integer.parseInt(input);
        
    }
    
    public static String validateString() {
        
        Scanner scanner = new Scanner(System.in);
        boolean flag = true;
        String input="";
        while(flag)
        {
            input = scanner.nextLine();
            char[] ch=input.toCharArray();
            for(int index = 0; index < input.length();index++)
            {
                if((ch[index]>=65 && ch[index]<=90) || (ch[index]>=97 && ch[index]<=122))
                {
                    flag = false;
                }         
                else
                {
                    System.out.println("String Can not Have a special Charater or NUmbers");
                    System.out.println("Please Enter Valid Detail");
                    flag = true;
                    break;
                }
            }
            
        }
        return input;
        
    }
}
